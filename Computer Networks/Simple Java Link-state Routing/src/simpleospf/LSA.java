package simpleospf;

import java.util.HashMap;

public class LSA implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8347775296970960340L;
	private int portNum;
	private int sequenceNum;
	private HashMap<Integer,Integer> linkState;

	public LSA(int portNum, int sequenceNum,
			HashMap<Integer, Integer> linkState) {
		this.portNum = portNum;
		this.sequenceNum = sequenceNum;
		this.linkState = linkState;
	}

	public int getPortNum() {
		return portNum;
	}

	public int getSequenceNum() {
		return sequenceNum;
	}
	
	public HashMap<Integer,Integer> getLinkState() {
		return linkState;
	}

	@Override
	public String toString() {
		return "LSA [portNum=" + portNum + ", sequenceNum=" + sequenceNum
				+ ", linkState=" + linkState + "]";
	}

}
