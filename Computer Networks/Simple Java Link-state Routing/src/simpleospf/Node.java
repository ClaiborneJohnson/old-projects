package simpleospf;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Execution: >Node [port number]
 * 
 * Node implements a simplified version of link-state routing on top of UDP.
 * 
 * Node prompts for user input in the format [neighbor port: Int]
 * [cost: Int] to learn its neighbors port numbers and cost. Input will end when
 * port number and cost are both -1. 
 * 
 * Example: 
 * Enter neighbors: 
 * 2222 3 
 * 4444 1
 * 5555 6 
 * -1 -1
 * 
 * @author Claiborne Johnson
 *
 */
public class Node {

	public static void main(String[] args) {
		// For scanning user input
		int inputPort, inputWeight;
		String inputLine;
		String[] inputTokens;
		Scanner scan;
		// For UDP communication
		int portNum, sequenceNum;
		DatagramSocket nodeSocket;
		InetAddress IPAddress;
		byte[] lsaBytes;
		DatagramPacket outgoingPacket, incomingPacket;
		// For managing view of network
		HashMap<Integer,Integer> nodeLinkState;
		ArrayList<LSA> lsaDatabase;
		LSA incomingLSA, nodeLSA;
		boolean lsaFound, newLSA;

		// Parse command line for port number
		portNum = -1;
		if (args.length > 0) {
			try {
				portNum = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
				System.err.println("Usage: Node <port number>");
				System.exit(1);
			}
		} else {
			System.err.println("Usage: Node <port number>");
			System.exit(1);
		}

		// Create UDP socket for communication
		nodeSocket = null;
		try {
			nodeSocket = new DatagramSocket(portNum);
		} catch (SocketException e) {
			System.err.println("Error creating socket:");
			e.printStackTrace();
			System.exit(1);
		}

		// Scan user input for neighbors and create link state
		nodeLinkState = new HashMap<Integer,Integer>();
		inputPort = -1;
		inputWeight = -1;
		scan = new Scanner(System.in);
		System.out.println("Enter neighbors (Stop with -1 -1):");
		while (true) {
			inputLine = scan.nextLine();
			try {
				inputTokens = inputLine.trim().split("\\s+");
				inputPort = Integer.parseInt(inputTokens[0]);
				inputWeight = Integer.parseInt(inputTokens[1]);
			} catch (Exception e) {
				System.out
						.println("Input format:<neighbor port: positive Int> <neighbor weight: positive Int>");
				System.out.println("Enter neighbors (Stop with -1 -1):");
				continue;
			}
			if (inputPort > 0 && inputWeight > 0) {
				nodeLinkState.put(inputPort, inputWeight);
			} else if (inputPort == -1 && inputWeight == -1) {
				break;
			} else {
				System.out
						.println("Input port and weight must be greater than 0.");
				System.out.println("Enter neighbors (Stop with -1 -1):");
				continue;
			}
		}
		scan.close();								

		// print initial link state at this node
		sequenceNum = 1;
		nodeLSA = new LSA(portNum, sequenceNum, nodeLinkState);
		System.out.println("Network view at node " + nodeLSA.getPortNum() + ":");
		System.out.println("\t" + nodeLSA);
		
		// perform LSA broadcast to all neighbors	
		IPAddress = null;
		try {
			IPAddress = InetAddress.getByName("localhost");
		} catch (UnknownHostException e1) {
			System.err
					.println("Error resolving IP address for localhost:");
			e1.printStackTrace();
			System.exit(1);
		}
		
		lsaBytes = null;
		try {
			lsaBytes = serializeLSA(nodeLSA);
		} catch (IOException e1) {
			System.err.println("Error serializing LSA:");
			e1.printStackTrace();
			System.exit(1);
		}
	
		for (int port : nodeLinkState.keySet()) {
			outgoingPacket = new DatagramPacket(lsaBytes, lsaBytes.length,
					IPAddress, port);
			try {
				nodeSocket.send(outgoingPacket);
			} catch (IOException e) {
				System.err.println("Error sending packet:");
				e.printStackTrace();
				System.exit(1);
			}
		}
		
		// compute SPT and routing table for initial view of the network
		lsaDatabase = new ArrayList<LSA>();	
		computeRoutingTable(nodeLSA, lsaDatabase);
		
		// loop reading and forwarding incoming LSAs
		byte[] incomingBytes = new byte[1024 * 1024];
		incomingLSA = null;
		while (true) {
			// read in the next LSA from the socket
			incomingPacket = new DatagramPacket(incomingBytes,
					incomingBytes.length);
			try {
				nodeSocket.receive(incomingPacket);			
			} catch (Exception e) {
				System.err.println("Error receiving packet:");
				e.printStackTrace();
				System.exit(1);
			}

			try {
				incomingLSA = deserializeLSA(incomingPacket.getData());
			} catch (Exception e1) {
				System.err.println("Error deserializing LSA:");
				e1.printStackTrace();
				System.exit(1);
			}

			// check LSA database for an LSA from this port
			lsaFound = false;
			newLSA = false;
			for (LSA lsa : lsaDatabase) {
				if (lsa.getPortNum() == incomingLSA.getPortNum()) {
					lsaFound = true;
					// if incoming LSA is newer, replace old version
					// note: for our simple implementation, this will never succeed
					if (lsa.getSequenceNum() < incomingLSA.getSequenceNum()) {
						lsaDatabase.remove(lsa);
						lsaDatabase.add(incomingLSA);
						newLSA = true;
					}
					break;
				}
			}
			
			// if LSA from this port not in database, add it
			if (!lsaFound) {
				lsaDatabase.add(incomingLSA);
				newLSA = true;
			}
			
			// if it's an LSA this node hasn't seen before, process it
			if (newLSA) {		
				// print network view
				System.out.println("Got new " + incomingLSA);
				System.out.println("Network view at node " + nodeLSA.getPortNum() + ":");
				System.out.println("\t" + nodeLSA);
				for (LSA lsa : lsaDatabase) {
					System.out.println("\t" + lsa);
				}			
				
				// forward LSA to neighbors
				for (int port : nodeLinkState.keySet()) {
					if (port != incomingPacket.getPort()) {
						try {
							nodeSocket.send(incomingPacket);
						} catch (IOException e) {
							System.err.println("Error forwarding packet:");
							e.printStackTrace();
							System.exit(1);
						}
					}
				}
				// recalculate SPT and routing table
				computeRoutingTable(nodeLSA, lsaDatabase);
			}
			
		} // end while
	} // end main
	
	/**
	 * Serialize an LSA into its byte representation
	 * @param lsa the LSA to be serialized
	 * @return a byte array containing the bytes of the LSA
	 * @throws IOException error creating the ObjectOutputStream or writing to it
	 */
	private static byte[] serializeLSA(LSA lsa) throws IOException {
		try (ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
				ObjectOutputStream objectOut = new ObjectOutputStream(
						byteArrayOut);) {
			objectOut.writeObject(lsa);
			byte[] lsaBytes = byteArrayOut.toByteArray();
			return lsaBytes;
		} catch (IOException e) {
			throw e;
		}
	}

	/**
	 * Deserialize the byte representation of an LSA
	 * @param lsaBytes the bytes of the LSA
	 * @return the LSA as an object
	 * @throws Exception error creating the ObjectInputStream or reading from it
	 */
	private static LSA deserializeLSA(byte[] lsaBytes) throws Exception {
		LSA lsa = null;
		try (ByteArrayInputStream byteArrayIn = new ByteArrayInputStream(lsaBytes);
				ObjectInputStream objectIn = new ObjectInputStream(byteArrayIn);) {
			lsa = (LSA) objectIn.readObject();
			return lsa;
		} catch (Exception e) {		
			throw e;
		}
	}

	/**
	 * Compute shortest path tree based on current view of the network
	 * and use it to print the routing table
	 * @param lsaDatabase an ArrayList containing the link states for other known nodes in the network
	 * @param nodeLSA the LSA containing the link state for the source node
	 */
	private static void computeRoutingTable(LSA nodeLSA, ArrayList<LSA> lsaDatabase) {
		ArrayList<LSA> processedLSAs = new ArrayList<LSA>();
		HashMap<Integer,Integer> distances = new HashMap<Integer,Integer>();
		HashMap<Integer,Integer> predecessors = new HashMap<Integer,Integer>();
		
		// Initialization: set distances to all other nodes to infinity		
		for (LSA lsa : lsaDatabase) {
			distances.put(lsa.getPortNum(), Integer.MAX_VALUE); // may cause problems if used in computation
		}
		
		// Initialization: set distances from Node to itself and neighbors
		processedLSAs.add(nodeLSA);		
		distances.put(nodeLSA.getPortNum(), 0);
		for (int neighborPort : nodeLSA.getLinkState().keySet()) {
			int neighborWeight = nodeLSA.getLinkState().get(neighborPort);
			distances.put(neighborPort, neighborWeight);
			predecessors.put(neighborPort, nodeLSA.getPortNum());
		}
		
		// Loop until all other nodes processed
		while (!(processedLSAs.containsAll(lsaDatabase))) {
			// find node not in N' with smallest distance
			LSA selectedLSA = null;
			for (LSA lsa : lsaDatabase) {
				if (!(processedLSAs.contains(lsa))) {
					if (selectedLSA == null) {
						selectedLSA = lsa;
					} else if (distances.get(lsa.getPortNum()) < distances.get(selectedLSA.getPortNum())) {
						selectedLSA = lsa;
					}
				}
			}
			processedLSAs.add(selectedLSA);
			
			// update distances for all nodes adjacent to selected node and not in processed set
			for (int neighborPort : selectedLSA.getLinkState().keySet()) {
				// check if the LSA corresponding with this port was already processed
				boolean isProcessed = false;
				for (LSA processedLSA : processedLSAs) {
					if (processedLSA.getPortNum() == neighborPort) {
						isProcessed = true;
					}
				}
				
				// if it wasn't processed, update the distance
				if (!isProcessed) {
					int weightToNeighbor = selectedLSA.getLinkState().get(neighborPort);
					if (distances.get(selectedLSA.getPortNum()) + weightToNeighbor < distances.getOrDefault(neighborPort, Integer.MAX_VALUE)) {
						distances.put(neighborPort, distances.get(selectedLSA.getPortNum()) + weightToNeighbor);
						predecessors.put(neighborPort, selectedLSA.getPortNum());
					}		
				}
			}
			
		} // end while
		
		// print out the routing table
		System.out.println("Routing table at node " + nodeLSA.getPortNum() + ":");
		System.out.println("\tDestination\tNext Node\tCost");
		
		for (int port : predecessors.keySet()) {
			int predecessor = port;
			while (predecessors.get(predecessor) != nodeLSA.getPortNum()) { // instead of null, replace with Node
				predecessor = predecessors.get(predecessor);
			}
			System.out.println("\t" + port + "\t\t" + predecessor + "\t\t" + distances.get(port));
		}
		System.out.println();
	} // end computeRoutingTable

} // end class
