
varying vec3 N;
varying vec3 L;
varying vec3 E;
varying vec3 H;
varying vec2 texcoordInterpolated;
attribute vec4 vertexPosition;
attribute vec3 vertexNormal;
attribute vec2 texcoord;
uniform vec4 lightPosition;
uniform mat4 Projection;
uniform mat4 ModelView;

void main()
{
    gl_Position = Projection * ModelView * vertexPosition;

    vec4 eyePosition = ModelView * vertexPosition;
    vec4 eyeLightPos = lightPosition;

    N = normalize(ModelView * vec4(vertexNormal,0)).xyz;
    L = normalize(eyeLightPos.xyz - eyePosition.xyz);
    E = -normalize(eyePosition.xyz);
    H = normalize(L + E);

texcoordInterpolated = texcoord;
}

