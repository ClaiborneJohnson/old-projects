/* FREEGLUT EXAMPLE CODE
* This is based upon smooth_opengl3.c - a sample that comes with freeglut. This uses GLEW instead for extensions, which is much better.
* smooth_opengl3.c, based on smooth.c, which is (c) by SGI.
* This program demonstrates smooth shading in a way which is fully
* OpenGL-3.1-compliant.
* A smooth shaded polygon is drawn in a 2-D projection.
*/

#include <GL/glew.h>
#include <GL/freeglut.h>

//glm library
#include <glm/glm.hpp>
#include <glm/gtx/transform2.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
//gli library
#include <gli/gli.hpp>
#include <gli/gtx/gl_texture2d.hpp>

#include "SOIL.h" // loads textures
#include "textfile.h" // used for initializing shaders
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
using namespace glm;

GLuint fgProjectionMatrixIndex;
GLuint fgModelViewMatrixIndex;
GLuint fgNormalIndex;
GLuint fgVertexIndex;
GLuint fgLightPositionIndex;
GLuint texcoordIndex;
GLuint textureUniIndex;

glm::mat4 projectionMatrix;
glm::mat4 modelviewMatrix;
glm::mat4 viewMatrix;
glm::vec4 lightPosition = vec4(0.0f,0.0f,-10.0f, 1.0f);

GLint left_click = GLUT_UP;
GLint right_click = GLUT_UP;
GLint xold;
GLint yold;
GLfloat eye_x = 0.0f; 
GLfloat eye_y = 0.0f;
GLfloat eye_z = -5.0f;
GLfloat center_x = 0.0f;
GLfloat center_y = 0.0f;
GLfloat center_z = 0.0f;
GLfloat rotate_x = 0.0f; 
GLfloat rotate_y = 0.0f; 
GLfloat rotate_z = 0.0f;
bool gridMode = true;
bool treeMode = false;
bool particleMode = false;
bool textMode = false;

int width = 500;  // default window width
int height = 500; // default window height
GLfloat aspect = (GLfloat) width / (GLfloat) height; // aspect ratio of the window

/* report GL errors, if any, to stderr */
void checkError(const char *functionName)
{
	GLenum error;
	while (( error = glGetError() ) != GL_NO_ERROR) {
		fprintf (stderr, "GL error 0x%X detected in %s\n", error, functionName);
	}
}

/* vertex array data for a colored 2D square, consisting of RGB color values
and XY coordinates */
const GLfloat varray[] = {
	0.0f, 1.0f, 0.0f,
	-0.2f, -0.2f, 0.0f, 1.0f,     // lower left vertex
	1.0f, 0.0f,

	0.0f, 0.0f, 1.0f, /* normal */
	0.2f, -0.2f, 0.0f,1.0f ,        /* lower right vertex */
	0.0f, 0.0f,

	0.0f, 0.0f, 1.0f, /* normal */
	-0.2f, 0.2f,0.0f,1.0f,         /* upper left vertex */
	1.0f, 1.0f,

	0.0f, 1.0f, 0.0f, /* normal */
	0.2f, 0.2f, 0.0f,1.0f,      /* upper right vertex*/
	0.0f, 1.0f /* texture components, Y's are flipped because of funky way openGL loads textures */
};

// indices into the vertex array - 4 vertices for 2 triangles
const GLuint Indices[] = {
	0,1,2,
	2,1,3
};

/* compile-time constants */
enum {
	numTextureComponents = 2,
	numColorComponents = 3, // there are 3 floats to define a color
	numVertexComponents = 4, // there are 2 floats to define the position of a vertex
	stride = sizeof(GLfloat) * (numColorComponents + numVertexComponents+numTextureComponents), // the space between all the information for a vertex
	numElements = sizeof(varray) / stride // number of vertices in the array
};

GLuint vertexBufferName;
GLuint IndexBufferId;
GLuint frameBufferId;
GLuint depthBufferId;
GLuint renderTexName;

void initFBO() {
	// FBO handle
	glGenFramebuffersEXT(1, &frameBufferId);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, frameBufferId);

	// render buffer for FBO
	glGenRenderbuffersEXT(1, &depthBufferId);
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, depthBufferId);

	// Allocate space for renderbuffer and attach depth buffer to bound frame buffer
	glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT,glutGet(GLUT_WINDOW_WIDTH),glutGet(GLUT_WINDOW_HEIGHT));
	glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, depthBufferId);

	// create texture to render to
	glActiveTexture(GL_TEXTURE0+1);
	glGenTextures(1, &renderTexName);
	glBindTexture(GL_TEXTURE_2D, renderTexName);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, glutGet(GLUT_WINDOW_WIDTH),glutGet(GLUT_WINDOW_HEIGHT), 0, GL_ALPHA, GL_UNSIGNED_BYTE, NULL);
	// glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, glutGet(GLUT_WINDOW_WIDTH),glutGet(GLUT_WINDOW_HEIGHT), 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	// connect the texture to the framebuffer
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, renderTexName, 0);
	glBindTexture(GL_TEXTURE_2D, 0);

	GLenum status = glCheckFramebufferStatusEXT(GL_FRAMEBUFFER_EXT);
	bool fboUsed;
	if (status != GL_FRAMEBUFFER_COMPLETE_EXT)
		fboUsed = false;
	checkError("initFBO");
}

// allocate space for our data in graphics card memory
void initBuffer(void)
{
	// IBO
	glGenBuffers(1, &IndexBufferId);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IndexBufferId);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW);

	// VBO
	glGenBuffers (1, &vertexBufferName);
	glBindBuffer (GL_ARRAY_BUFFER, vertexBufferName);
	glBufferData (GL_ARRAY_BUFFER, sizeof(varray), varray, GL_STATIC_DRAW);

	// FBO
	initFBO();

	checkError ("initBuffer");
}

GLuint textureSquare;
GLuint textureTree;
GLuint textureParticle;
GLuint textureText;
// initialize textures
void initTexture(){

	//	glEnable(GL_TEXTURE_2D);
	textureSquare = SOIL_load_OGL_texture("textures/square.jpg",
		SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID,SOIL_FLAG_INVERT_Y);
	textureTree = gli::createTexture2D("textures/tree.tga");
	textureParticle = SOIL_load_OGL_texture("textures/smoketex.jpg",
		SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID,SOIL_FLAG_INVERT_Y);
	textureText = SOIL_load_OGL_texture("textures/boingzoomdakota.jpg",
		SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID,SOIL_FLAG_INVERT_Y);

	glBindTexture(GL_TEXTURE_2D, textureSquare);	
	glActiveTexture( GL_TEXTURE0 );
}

///jpq
void compileAndCheck(GLuint shader)
{
	GLint status;
	glCompileShader (shader);
	glGetShaderiv (shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE) {
		GLint infoLogLength;
		char *infoLog;
		glGetShaderiv (shader, GL_INFO_LOG_LENGTH, &infoLogLength);
		infoLog = (char*) malloc (infoLogLength);
		glGetShaderInfoLog (shader, infoLogLength, NULL, infoLog);
		fprintf (stderr, "compile log: %s\n", infoLog);
		free (infoLog);
	}
}

GLuint compileShaderSource(GLenum type, GLsizei count, const char **string)
{
	GLuint shader = glCreateShader (type);
	glShaderSource (shader, count,  string, NULL);
	compileAndCheck (shader);
	return shader;
}

void linkAndCheck(GLuint program)
{
	GLint status;
	glLinkProgram (program);
	glGetProgramiv (program, GL_LINK_STATUS, &status);
	if (status == GL_FALSE) {
		GLint infoLogLength;
		char *infoLog;
		glGetProgramiv (program, GL_INFO_LOG_LENGTH, &infoLogLength);
		infoLog = (char*) malloc (infoLogLength);
		glGetProgramInfoLog (program, infoLogLength, NULL, infoLog);
		fprintf (stderr, "link log: %s\n", infoLog);
		free (infoLog);
	}
}

GLuint createProgram(GLuint vertexShader, GLuint fragmentShader)
{
	GLuint program = glCreateProgram ();
	if (vertexShader != 0) {
		glAttachShader (program, vertexShader);
	}
	if (fragmentShader != 0) {
		glAttachShader (program, fragmentShader);
	}
	linkAndCheck (program);
	return program;
}

void initShader(void)
{
	char * vertexShaderSource = textFileRead("./shaders/phong.vert");
	GLuint vertexShader =
		compileShaderSource (GL_VERTEX_SHADER, 1, (const char **) &vertexShaderSource);

	char * fragmentShaderSource = textFileRead("./shaders/phong.frag");
	GLuint fragmentShader =
		compileShaderSource (GL_FRAGMENT_SHADER, 1,(const char **) &fragmentShaderSource);

	GLuint program = createProgram (vertexShader, fragmentShader);

	glUseProgram (program);
	//jpq 
	fgProjectionMatrixIndex = glGetUniformLocation(program, "Projection");
	fgModelViewMatrixIndex = glGetUniformLocation(program, "ModelView");
	fgLightPositionIndex = glGetUniformLocation(program, "lightPosition");
	textureUniIndex =   glGetUniformLocation(program, "tex");
	fgNormalIndex = glGetAttribLocation(program, "vertexNormal");
	glEnableVertexAttribArray (fgNormalIndex);

	fgVertexIndex = glGetAttribLocation(program, "vertexPosition");
	glEnableVertexAttribArray (fgVertexIndex);

	texcoordIndex = glGetAttribLocation(program, "texcoord");
	glEnableVertexAttribArray (texcoordIndex);
	checkError ("initShader");
}

void initRendering(void)
{
	glClearColor (0.0, 0.0, 0.0, 0.0); // change for background color
	checkError ("initRendering");
}

void init(void) 
{
	initTexture();
	initBuffer ();
	initShader ();
	initRendering ();
}

void dumpInfo(void)
{
	printf ("Vendor: %s\n", glGetString (GL_VENDOR));
	printf ("Renderer: %s\n", glGetString (GL_RENDERER));
	printf ("Version: %s\n", glGetString (GL_VERSION));
	printf ("GLSL: %s\n", glGetString (GL_SHADING_LANGUAGE_VERSION));
	checkError ("dumpInfo");
}

const GLvoid *bufferObjectPtr (GLsizei index)
{
	return (const GLvoid *) (((char *) NULL) + index);
}

void Square(void)
{

	const GLvoid * i =  bufferObjectPtr (0);

	glUniformMatrix4fv (fgProjectionMatrixIndex, 1, GL_FALSE, &projectionMatrix[0][0]);
	glUniformMatrix4fv (fgModelViewMatrixIndex, 1, GL_FALSE, &modelviewMatrix[0][0]);
	glUniform4fv(fgLightPositionIndex,1,&lightPosition[0]);
	glUniform1i(textureUniIndex,0);


	glBindBuffer (GL_ARRAY_BUFFER, vertexBufferName);
	glVertexAttribPointer (fgNormalIndex, numColorComponents, GL_FLOAT, GL_FALSE,
		stride, bufferObjectPtr (0));
	glVertexAttribPointer (fgVertexIndex, numVertexComponents, GL_FLOAT, GL_FALSE,
		stride, bufferObjectPtr (sizeof(GLfloat) * numColorComponents));
	glVertexAttribPointer (texcoordIndex, numTextureComponents, GL_FLOAT, GL_FALSE,
		stride, bufferObjectPtr (sizeof(GLfloat) * ( numColorComponents+numVertexComponents)));
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	checkError ("Square");
}

void Grid() {
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, frameBufferId);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureSquare);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Square();

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	Square();

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, renderTexName);

	int i;
	int j;
	int k;

	for (i = 0; i < 3; i++)
		for (j = 0; j < 3; j++)
			for (k = 0; k < 3; k++) {
				modelviewMatrix = viewMatrix * translate(i - 1.0f, j - 1.0f, k - 1.0f);
				/* the bottom rows are axis aligned billboards (trees) */
				if (j == 0) { // will rotate y before drawing
					modelviewMatrix *= rotate(rotate_x, 0.0f, -1.0f, 0.0f);
				}

				/* the middle rows are world aligned billboards (particles) */
				if (j == 1) {   // will rotate y and x before drawing
					modelviewMatrix *= rotate(rotate_x, 0.0f, -1.0f, 0.0f) * 
						rotate(rotate_y, -1.0f, 0.0f, 0.0f);			
				}

				/* the top rows are screen aligned billboards (text)*/
				if (j == 2) { // will rotate z,y,x before drawing
					modelviewMatrix *= rotate(rotate_x, 0.0f, -1.0f, 0.0f) * 
						rotate(rotate_y, -1.0f, 0.0f, 0.0f) *
						rotate(rotate_z, 0.0f, 0.0f, -1.0f);
				}
				Square(); // draw billboard
			}

}

void Tree() {
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, frameBufferId);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureTree);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Square();

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	Square();

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, renderTexName);

	int i;
	int j;

	for (i = 0; i < 10; i++)
		for (j = 0; j < 10; j++) {
			modelviewMatrix = viewMatrix;
			modelviewMatrix *= translate(0.3f * i,0.0f,0.3f * j) *
				rotate(rotate_x, 0.0f, -1.0f, 0.0f);
			Square();

			modelviewMatrix = viewMatrix;
			modelviewMatrix *= translate(-0.3f * i,0.0f,-0.3f * j) *
				rotate(rotate_x, 0.0f, -1.0f, 0.0f);
			Square();

			modelviewMatrix = viewMatrix;
			modelviewMatrix *= translate(-0.3f * i,0.0f,0.3f * j) *
				rotate(rotate_x, 0.0f, -1.0f, 0.0f);
			Square();

			modelviewMatrix = viewMatrix;
			modelviewMatrix *= translate(0.3f * i,0.0f,-0.3f * j) *
				rotate(rotate_x, 0.0f, -1.0f, 0.0f);
			Square();
		}
}

void Particle() {
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, frameBufferId);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureParticle);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Square();

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	Square();

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, renderTexName);

	modelviewMatrix = viewMatrix;
	modelviewMatrix *= rotate(rotate_x, 0.0f, -1.0f, 0.0f) * 
		rotate(rotate_y, -1.0f, 0.0f, 0.0f);
	Square();

	modelviewMatrix = viewMatrix;
	modelviewMatrix *= translate(-1.0f,0.0f,0.0f) * rotate(rotate_x, 0.0f, -1.0f, 0.0f) * 
		rotate(rotate_y, -1.0f, 0.0f, 0.0f);
	Square();

	modelviewMatrix = viewMatrix;
	modelviewMatrix *= translate(1.0f,0.0f,0.0f) * rotate(rotate_x, 0.0f, -1.0f, 0.0f) * 
		rotate(rotate_y, -1.0f, 0.0f, 0.0f);
	Square();
}

void Text() {
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, frameBufferId);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureText);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Square();

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	Square();

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, renderTexName);

	modelviewMatrix = viewMatrix;
	modelviewMatrix *= rotate(rotate_x, 0.0f, -1.0f, 0.0f) *
		rotate(rotate_y, -1.0f, 0.0f, 0.0f) *
		rotate(rotate_z, 0.0f, 0.0f, -1.0f);
	Square();

	modelviewMatrix = viewMatrix;
	modelviewMatrix *= translate(1.0f,0.0f,0.0f) * rotate(rotate_x, 0.0f, -1.0f, 0.0f) *
		rotate(rotate_y, -1.0f, 0.0f, 0.0f) *
		rotate(rotate_z, 0.0f, 0.0f, -1.0f);
	Square();

	modelviewMatrix = viewMatrix;
	modelviewMatrix *= translate(-1.0f,0.0f,0.0f) * rotate(rotate_x, 0.0f, -1.0f, 0.0f) *
		rotate(rotate_y, -1.0f, 0.0f, 0.0f) *
		rotate(rotate_z, 0.0f, 0.0f, -1.0f);
	Square();
}

void display(void)
{	

	viewMatrix = glm::lookAt(vec3(eye_x,eye_y,eye_z),vec3(center_x,center_y,center_z),vec3(0.0f,1.0f,0.0f)); 
	viewMatrix *= rotate(rotate_z, 0.0f, 0.0f, 1.0f) * rotate(rotate_y, 1.0f, 0.0f, 0.0f) * 
		rotate(rotate_x, 0.0f, 1.0f, 0.0f);

	if (gridMode) { 
		Grid();
	}

	else if (treeMode) {
		Tree();
	}

	else if (particleMode) { 
		Particle();
	}

	else if (textMode) { 
		Text();
	}

	glutSwapBuffers ();
	checkError ("display");
}


void idle(){
	glutPostRedisplay();
	//glutSwapBuffers ();
}


void reshape (int w, int h)
{
	glViewport (0, 0, (GLsizei) w, (GLsizei) h);

	width = w;
	height = h;
	aspect = (GLfloat) width / (GLfloat) height;

	GLfloat left = -2.0f;
	GLfloat right = 2.0f;
	GLfloat bottom = -2.0f;
	GLfloat top = 2.0f;

	if (aspect <= 1.0) {
		bottom /= aspect;
		top /= aspect;
	}
	else {
		left *= aspect;
		right *= aspect;
	}
	//	projectionMatrix = glm::ortho(left,right,bottom,top,.01f,15.0f); 
	projectionMatrix = glm::perspective(45.0f, aspect, .01f, 15.0f); 
	checkError ("reshape");
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key) {
	case 27:
		exit(0);
		break;
	case 'w':
	case 'W':
		// translate camera forward
		eye_z += 1.0f;
		center_z += 1.0f;
		break;
	case 'q':
	case 'Q':
		// translate camera left
		eye_x += 1.0f;
		center_x += 1.0f;
		break;
	case 's':
	case 'S':
		// translate camera backward
		eye_z -= 1.0f;
		center_z -= 1.0f;
		break;
	case 'e':
	case 'E':
		// translate camera right
		eye_x -= 1.0f;
		center_x -= 1.0f;
		break;
	case 'a':
	case 'A':
		// turn camera left
		//	if (center_x == 359.0f) center_x = 0.0f;
		//	center_x += 1.0f;
		//	center_x *= -1.0f;
		break;
	case 'd':
	case 'D':
		// turn camera right
		if (center_x == -359.0f) center_x = 0.0f;
		center_x -= 1.0f;
		break;
	case 'r': 
	case 'R':
		// reset camera
		eye_x = 0.0f;
		eye_y = 0.0f;
		eye_z = -5.0f;
		center_x = 0.0f;
		center_y = 0.0f;
		center_z = 0.0f;
		rotate_x = 0.0f;
		rotate_y = 0.0f;
		rotate_z = 0.0f;
		break;
	case '1':
		// grid mode 
		gridMode = true;
		treeMode = false;
		particleMode = false;
		textMode = false;
		break;
	case '2':
		// tree mode
		gridMode = false;
		treeMode = true;
		particleMode = false;
		textMode = false;
		Square();
		break;
	case '3':
		// particle mode
		gridMode = false;
		treeMode = false;
		particleMode = true;
		textMode = false;
		break;
	case '4':
		// text mode
		gridMode = false;
		treeMode = false;
		particleMode = false;
		textMode = true;
		break;
	}
}

void mouse(int button, int state, int x, int y) 
{
	if (GLUT_LEFT_BUTTON == button)
		left_click = state;
	if (GLUT_RIGHT_BUTTON == button)
		right_click = state;
	xold = x;
	yold = y;
}

void motion(int x, int y) 
{
	if (GLUT_DOWN == left_click)
	{
		rotate_y = rotate_y + (y - yold) / 5.0;
		rotate_x = rotate_x + (x - xold) / 5.0;
		if (rotate_y > 90)
			rotate_y = 90;
		if (rotate_y < -90)
			rotate_y = -90;
		glutPostRedisplay ();
	}
	if (GLUT_DOWN == right_click)
	{
		rotate_z = rotate_z + (x - xold) / 5.0;
		glutPostRedisplay ();
	}
	xold = x;
	yold = y;
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize (500, 500); 
	glutInitWindowPosition (100, 100);
	glutCreateWindow (argv[0]);
	glewInit();
	dumpInfo ();
	init ();
	glutDisplayFunc(display); 
	glutIdleFunc(idle); 
	glutReshapeFunc(reshape);
	glutKeyboardFunc (keyboard);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glEnable(GL_DEPTH_TEST);
//	glEnable(GL_BLEND);
	glEnable(GL_ALPHA_TEST);
	glAlphaFunc(GL_GREATER, 0.0);
 //   glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glutMainLoop();
	return 0;
}
